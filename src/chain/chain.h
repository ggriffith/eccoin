// This file is part of the Eccoin project
// Copyright (c) 2009-2010 Satoshi Nakamoto
// Copyright (c) 2009-2016 The Bitcoin Core developers
// Copyright (c) 2014-2018 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#ifndef BITCOIN_CHAIN_H
#define BITCOIN_CHAIN_H

#include "arith_uint256.h"
#include "block.h"
#include "blockindex.h"
#include "pow.h"
#include "tinyformat.h"
#include "uint256.h"

#include <atomic>
#include <vector>

/** An in-memory indexed chain of blocks. */
class CChain
{
private:
    mutable CSharedCriticalSection cs_chainLock;

    std::vector<CBlockIndex *> vChain;

    std::atomic<CBlockIndex *> tip;

    bool _Contains(const CBlockIndex *pindex) const
    {
        if (pindex == nullptr || pindex->nHeight < 0 || pindex->nHeight >= (int)vChain.size())
        {
            return false;
        }
        // We can return this outside of the lock because CBlockIndex objects are never deleted
        return (vChain[pindex->nHeight] == pindex);
    }

public:
    CChain() : tip(nullptr) {}
    ~CChain()
    {
        vChain.clear();
        tip = nullptr;
    }
    /** Returns the index entry for the genesis block of this chain, or NULL if none. */
    CBlockIndex *Genesis() const { return (*this)[0]; }
    /** Returns the index entry for the tip of this chain, or NULL if none. */
    CBlockIndex *Tip() const { return tip; }
    /** Returns the index entry at a particular height in this chain, or NULL if no such height exists. */
    CBlockIndex *operator[](int nHeight) const
    {
        READLOCK(cs_chainLock);
        if (nHeight < 0 || nHeight >= (int)vChain.size())
        {
            return nullptr;
        }
        return vChain[nHeight];
    }

    /** Compare two chains efficiently. */
    friend bool operator==(const CChain &a, const CChain &b)
    {
        READLOCK(a.cs_chainLock);
        READLOCK(b.cs_chainLock);
        return a.vChain.size() == b.vChain.size() && a.vChain[a.vChain.size() - 1] == b.vChain[b.vChain.size() - 1];
    }

    void operator=(const CChain &a)
    {
        vChain = a.vChain;
        tip.store(a.tip.load());
    }

    /** Efficiently check whether a block is present in this chain. */
    bool Contains(const CBlockIndex *pindex) const { return (*this)[pindex->nHeight] == pindex; }
    /** Find the successor of a block in this chain, or NULL if the given index is not found or is the tip. */
    CBlockIndex *Next(const CBlockIndex *pindex) const
    {
        READLOCK(cs_chainLock);
        if (pindex)
        {
            int nextHeight = pindex->nHeight + 1;
            if (_Contains(pindex) && nextHeight >= 0 && nextHeight < (int)vChain.size())
            {
                return vChain[nextHeight];
            }
        }
        return nullptr;
    }

    /** Return the maximal height in the chain. Is equal to chain.Tip() ? chain.Tip()->nHeight : -1. */
    int Height() const { return tip.load() ? tip.load()->nHeight : -1; }
    /** Set/initialize a chain with a given tip. */
    void SetTip(CBlockIndex *pindex);

    /** Return a CBlockLocator that refers to a block in this chain (by default the tip). */
    CBlockLocator GetLocator(const CBlockIndex *pindex = nullptr) const;

    /** Find the last common block between this chain and a block index entry. */
    const CBlockIndex *FindFork(const CBlockIndex *pindex) const;
};

#endif // BITCOIN_CHAIN_H
