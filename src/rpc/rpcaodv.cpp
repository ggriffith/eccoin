// This file is part of the Eccoin project
// Copyright (c) 2019 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "beta.h"
#include "roam/aodv.h"
#include "roam/packetmanager.h"
#include "rpcserver.h"
#include "util/logger.h"
#include "util/utilstrencodings.h"
#include <univalue.h>

#include "main.h"

#include <sstream>

UniValue getaodvtable(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 0)
        throw std::runtime_error(
            "getaodvtable\n"
            "\nreturns the aodv routing table.\n"
            "\nResult:\n"
            "{\n"
            "   \"mapidkey\" : {\n"
            "       \"NodeId : Key,\"\n"
            "       ...\n"
            "   },\n"
            "   \"mapkeyid\" : {\n"
            "       \"Key : NodeId,\"\n"
            "       ...\n"
            "   }\n"
            "}\n"
            "\nExamples:\n" +
            HelpExampleCli("getaodvtable", "") +
            HelpExampleRpc("getaodvtable", "")
        );
    std::map<NodeId, std::set<CPubKey> > IdKey;
    std::map<CPubKey, std::set<NodeId> > KeyId;
    g_aodvtable.GetRoutingTables(IdKey, KeyId);
    UniValue obj(UniValue::VOBJ);
    UniValue IdKeyObj(UniValue::VOBJ);
    for (auto &entry : IdKey)
    {
        for (auto &path : entry.second)
        {
            IdKeyObj.push_back(Pair(std::to_string(entry.first), path.Raw64Encoded()));
        }
    }
    UniValue KeyIdObj(UniValue::VOBJ);
    for (auto &entry : KeyId)
    {
        for (auto &path : entry.second)
        {
            KeyIdObj.push_back(Pair(entry.first.Raw64Encoded(), path));
        }
    }
    obj.push_back(Pair("mapidkey", IdKeyObj));
    obj.push_back(Pair("mapkeyid", KeyIdObj));
    return obj;
}


UniValue getaodvkeyentry(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 1)
        throw std::runtime_error(
            "getaodvkeyentry \"keyhash\" \n"
            "\nChecks AODV routing table for a key with a hash that matches keyhash, returns its NodeId.\n"
            "\nArguments:\n"
            "1. \"keyhash\"   (string, required) The hash of the key of the desired entry\n"
            "\nNote: This call is fairly expensive due to number of hashes being done.\n"
            "\nExamples:\n"+
            HelpExampleCli("getaodvkeyentry", "\"keyhash\"") +
            HelpExampleRpc("getaodvkeyentry", "\"keyhash\""));

    UniValue obj(UniValue::VOBJ);
    obj.push_back(Pair("Error", "this rpc call is currently disabled"));
    return obj;
}

UniValue getaodvidentry(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 1)
        throw std::runtime_error(
            "getaodvidentry \"nodeid\" \n"
            "\nChecks AODV routing table for the desired nodeid, returns its keys hash.\n"
            "\nArguments:\n"
            "1. \"nodeid\"   (number, required) The nodeid of the desired entry\n"
            "\nExamples:\n"+
            HelpExampleCli("getaodvidentry", "12") +
            HelpExampleRpc("getaodvidentry", "32"));

    UniValue obj(UniValue::VOBJ);
    obj.push_back(Pair("Error", "this rpc call is currently disabled"));
    return obj;
}

UniValue getroutingpubkey(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 0)
        throw std::runtime_error(
            "getroutingpubkey\n"
            "\nreturns the routing public key used by this node.\n"
            "\nResult:\n"
            "\"Key\" (string)\n"
            "\nExamples:\n" +
            HelpExampleCli("getroutingpubkey", "") +
            HelpExampleRpc("getroutingpubkey", "")
        );

    if (!g_connman)
    {
        throw JSONRPCError(RPC_CLIENT_P2P_DISABLED, "Error: Peer-to-peer functionality missing or disabled");
    }

    return g_connman->GetPublicTagPubKey().Raw64Encoded();
}


UniValue findroute(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 1)
        throw std::runtime_error(
            "findroute\n"
            "\nattempts to find a route to the node with the given pub key.\n"
            "\nResult:\n"
            "\"None\n"
            "\nExamples:\n" +
            HelpExampleCli("findroute", "1139d39a984a0ff431c467f738d534c36824401a4735850561f7ac64e4d49f5b") +
            HelpExampleRpc("findroute", "1139d39a984a0ff431c467f738d534c36824401a4735850561f7ac64e4d49f5b")
        );

    if (!g_connman)
    {
        throw JSONRPCError(RPC_CLIENT_P2P_DISABLED, "Error: Peer-to-peer functionality missing or disabled");
    }

    bool fInvalid = false;
    std::vector<unsigned char> vPubKey = DecodeBase64(params[0].get_str().c_str(), &fInvalid);
    if (fInvalid)
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Malformed pubkey base64 encoding");
    }
    if (g_aodvtable.HaveRoute(CPubKey(vPubKey.begin(), vPubKey.end())))
    {
        return NullUniValue;
    }
    uint64_t nonce = 0;
    GetStrongRandBytes((uint8_t *)&nonce, sizeof(nonce));
    CPubKey key;
    RequestRouteToPeer(g_connman.get(), key, nonce, CPubKey(vPubKey.begin(), vPubKey.end()));
    return NullUniValue;
}

UniValue haveroute(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 1)
        throw std::runtime_error(
            "haveroute\n"
            "\nchecks if a route to the node with the given pub key is known.\n"
            "\nResult:\n"
            "\"true/false\n"
            "\nExamples:\n" +
            HelpExampleCli("haveroute", "1139d39a984a0ff431c467f738d534c36824401a4735850561f7ac64e4d49f5b") +
            HelpExampleRpc("haveroute", "1139d39a984a0ff431c467f738d534c36824401a4735850561f7ac64e4d49f5b")
        );

    if (!g_connman)
    {
        throw JSONRPCError(RPC_CLIENT_P2P_DISABLED, "Error: Peer-to-peer functionality missing or disabled");
    }
    bool fInvalid = false;
    std::vector<unsigned char> vPubKey = DecodeBase64(params[0].get_str().c_str(), &fInvalid);
    if (fInvalid)
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Malformed pubkey base64 encoding");
    }
    return g_aodvtable.HaveRoute(CPubKey(vPubKey.begin(), vPubKey.end()));
}

UniValue sendpacket(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 3)
    {
        throw std::runtime_error(
            "sendpacket\n"
            "\nattempts to send a network packet to the destination\n"
            "\nArguments:\n"
            "1. \"key\"   (string, required) The key of the desired recipient\n"
            "2. \"protocolId\"   (number, required) The id of the protocol being used for the data\n"
            "3. \"Data\"   (vector of bytes, required) The desired data to be sent \n"
            "\nExamples:\n" +
            HelpExampleCli("sendpacket", "\"1139d39a984a0ff431c467f738d534c36824401a4735850561f7ac64e4d49f5b\" 1 \"this is example data\"") +
            HelpExampleRpc("sendpacket", "\"1139d39a984a0ff431c467f738d534c36824401a4735850561f7ac64e4d49f5b\", 1, \"this is example data\"")
        );
    }
    bool fInvalid = false;
    std::vector<unsigned char> vPubKey = DecodeBase64(params[0].get_str().c_str(), &fInvalid);
    // TODO : import unsigned values for univalue from upstream, change thse calls to get_uint8
    uint16_t nProtocolId = (uint16_t)params[1].get_int();
    std::vector<uint8_t> vData = StrToBytes(params[2].get_str());

    bool result = g_packetman.SendPacket(vPubKey, nProtocolId, vData);
    return result;
}

UniValue registerbuffer(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 1)
    {
        throw std::runtime_error(
            "registerbuffer\n"
            "\nattempts to register the buffer for a network service protocol to a specific and returns the pubkey needed to read the buffer\n"
            "\nArguments:\n"
            "1. \"protocolId\"   (number, required) The id of the protocol being requested\n"
            "\nExamples:\n" +
            HelpExampleCli("registerbuffer", "1 \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\"") +
            HelpExampleRpc("registerbuffer", "1, \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\"")
        );
    }
    uint16_t nProtocolId = (uint16_t)params[0].get_int();
    UniValue obj(UniValue::VOBJ);
    std::string pubkey;
    if (g_packetman.RegisterBuffer(nProtocolId, pubkey))
    {
        return pubkey;
    }
    throw JSONRPCError(RPC_INTERNAL_ERROR, "registerbuffer: failed to register buffer");
}

UniValue getbuffer(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 2)
    {
        throw std::runtime_error(
            "getbuffer\n"
            "\nattempts to get the buffer for a network service protocol\n"
            "\nArguments:\n"
            "1. \"protocolId\"   (number, required) The id of the protocol being requested\n"
            "2. \"signature\"   (string, required) The authentication signature required to request the buffer\n"
            "\nExamples:\n" +
            HelpExampleCli("getbuffer", "1 \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\"") +
            HelpExampleRpc("getbuffer", "1, \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\"")
        );
    }
    uint16_t nProtocolId = (uint16_t)params[0].get_int();
    std::string sig = params[1].get_str();
    std::vector<CPacket> bufferData;
    UniValue obj(UniValue::VOBJ);
    if (g_packetman.GetBuffer(nProtocolId, bufferData, sig))
    {
        uint64_t counter = 0;
        for (auto &entry: bufferData)
        {
            std::stringstream hexstream;
            hexstream << std::hex;
            for (uint8_t &byte : entry.GetData())
            {
                hexstream << std::setw(2) << std::setfill('0') << static_cast<int>(byte);
            }
            obj.push_back(Pair(std::to_string(counter), hexstream.str()));
            counter++;
        }
    }
    else
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "getbuffer failed, buffer not registered or invalid signature");
    }
    return obj;
}

UniValue resetbuffertimeout(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 2)
    {
        throw std::runtime_error(
            "resetbuffertimeout\n"
            "\na buffer keepalive, reset the buffer timeout timer.\n"
            "\nArguments:\n"
            "1. \"protocolId\"   (number, required) The id of the protocol being requested\n"
            "2. \"signature\"   (string, required) The authentication signature required to request the buffer\n"
            "\nExamples:\n" +
            HelpExampleCli("resetbuffertimeout", "1 \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\"") +
            HelpExampleRpc("resetbuffertimeout", "1, \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\"")
        );
    }
    uint16_t nProtocolId = (uint16_t)params[0].get_int();
    std::string sig = params[1].get_str();
    if (g_packetman.ResetBufferTimeout(nProtocolId, sig))
    {
        return true;
    }
    throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "resetbuffertimeout failed, buffer not registered or invalid signature");
}

UniValue releasebuffer(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 2)
    {
        throw std::runtime_error(
            "releasebuffer\n"
            "\nattempts to release the buffer for a network service protocol\n"
            "\nArguments:\n"
            "1. \"protocolId\"   (number, required) The id of the protocol being requested\n"
            "2. \"signature\"   (string, required) The authentication signature required to release the buffer\n"
            "\nExamples:\n" +
            HelpExampleCli("releasebuffer", "1 \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\"") +
            HelpExampleRpc("releasebuffer", "1, \"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\"")
        );
    }
    uint16_t nProtocolId = (uint8_t)params[0].get_int();
    std::string sig = params[1].get_str();
    return g_packetman.ReleaseBuffer(nProtocolId, sig);
}

UniValue buffersignmessage(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 2)
    {
        throw std::runtime_error(
            "buffersignmessage\n"
            "\nsigns a message with a provided base64 encoded pubkey\n"
            "\nArguments:\n"
            "1. \"pubkey\"   (string, required) The base64 encoded pubkey\n"
            "2. \"message\"  (string, required) The message to be signed\n"
            "\nResult:\n"
            "\"signature\"          (string) The signature of the message encoded in base 64\n"
            "\nExamples:\n" +
            HelpExampleCli("buffersignmessage", "\"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\" \"my message\"") +
            HelpExampleRpc("buffersignmessage", "\"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\", \"my message\"")
        );
    }

    std::string strPubKey = params[0].get_str();
    std::string strMessage = params[1].get_str();

    CHashWriter ss(SER_GETHASH, 0);
    ss << strMessage;

    std::vector<unsigned char> vchSig;
    // restore the pubkey from the base64 encoded pubkey provided
    CPubKey pubkey(strPubKey);
    CKey key;
    if (!g_packetman.GetBufferKey(pubkey, key))
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "buffersignmessage: lookup failed");
    }

    if (!key.SignCompact(ss.GetHash(), vchSig))
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "buffersignmessage: Sign failed");
    }
    return EncodeBase64(&vchSig[0], vchSig.size());
}

std::string tagsignmessage(const std::string &strMessage)
{
    CHashWriter ss(SER_GETHASH, 0);
    ss << strMessageMagic;
    ss << strMessage;

    if (!g_connman)
    {
        throw JSONRPCError(RPC_CLIENT_P2P_DISABLED, "Error: Peer-to-peer functionality missing or disabled");
    }

    CRoutingTag pubtag;
    if (!g_connman->tagstore->GetTag(g_connman->tagstore->GetCurrentPublicTagPubKey().GetID(), pubtag))
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Sign failed, Tag error");
    }
    std::vector<unsigned char> vchSig;
    if (!pubtag.SignCompact(ss.GetHash(), vchSig))
    {
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Sign failed");
    }
    return EncodeBase64(&vchSig[0], vchSig.size());
}

UniValue tagsignmessage(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 1)
    {
        throw std::runtime_error(
            "tagsignmessage\n"
            "\nsigns a message with your public tag\n"
            "\nArguments:\n"
            "1. \"message\"   (string, required) The message to be signed\n"
            "\nResult:\n"
            "\"signature\"          (string) The signature of the message encoded in base 64\n"
            "\nExamples:\n" +
            HelpExampleCli("tagsignmessage", "hello. sign this message") +
            HelpExampleRpc("tagsignmessage", "hello. sign this message")
        );
    }

    std::string strMessage = params[0].get_str();
    return tagsignmessage(strMessage);
}

bool tagverifymessage(const std::string &strPubKey_base64, const std::string &strSign, const std::string &strMessage)
{
    CHashWriter ss(SER_GETHASH, 0);
    ss << strMessageMagic;
    ss << strMessage;

    bool fInvalid = false;
    std::vector<unsigned char> vchSig = DecodeBase64(strSign.c_str(), &fInvalid);

    if (fInvalid)
        throw JSONRPCError(RPC_INVALID_ADDRESS_OR_KEY, "Malformed base64 encoding");

    CPubKey pubcompare(strPubKey_base64);
    CPubKey pubkey;
    if (!pubkey.RecoverCompact(ss.GetHash(), vchSig))
        return false;

    return (pubkey.GetID() == pubcompare.GetID());
}

UniValue tagverifymessage(const UniValue &params, bool fHelp)
{
    if (!IsBetaEnabled())
    {
        return "This rpc call requires beta features to be enabled (-beta or beta=1) \n";
    }

    if (fHelp || params.size() != 3)
        throw std::runtime_error(
            "tagverifymessage \"ecc address\" \"signature\" \"message\"\n"
            "\nVerify a signed message\n"
            "\nArguments:\n"
            "1. \"pubkey\"  (string, required) The base64 encoded tag pubkey to use for the signature\n"
            "2. \"signature\"       (string, required) The signature provided by the signer in base 64 encoding (see "
            "signmessage).\n"
            "3. \"message\"         (string, required) The message that was signed.\n"
            "\nResult:\n"
            "true|false   (boolean) If the signature is verified or not.\n"
            "\nExamples:\n"
            "\nUnlock the wallet for 30 seconds\n" +
            HelpExampleCli("walletpassphrase", "\"mypassphrase\" 30") + "\nCreate the signature\n" +
            HelpExampleCli("signmessage", "\"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\" \"my message\"") +
            "\nVerify the signature\n" +
            HelpExampleCli("tagverifymessage", "\"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\" \"signature\" \"my message\"") +
            "\nAs json rpc\n" +
            HelpExampleRpc("tagverifymessage", "\"BHcOxO9SxZshlmXffMFdJYuAXqusM3zVS7Ary66j5SiupLsnGeMONwmM/qG6zIEJpoGznWtmFFZ63mo5YXGWBcU=\", \"signature\", \"my message\""));

    std::string strPubKey_base64 = params[0].get_str();
    std::string strSign = params[1].get_str();
    std::string strMessage = params[2].get_str();
    return tagverifymessage(strPubKey_base64, strSign, strMessage);
}
