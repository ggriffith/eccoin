// This file is part of the Eccoin project
// Copyright (c) 2020 The Eccoin developers
// Distributed under the MIT software license, see the accompanying
// file COPYING or http://www.opensource.org/licenses/mit-license.php.

#include "requestmanager.h"

#include "chain/chainman.h"
#include "connman.h"
#include "messages.h"

extern std::atomic<int> nPreferredDownload;

extern std::atomic<bool> fImporting;
extern std::atomic<bool> fReindex;

extern bool AlreadyHave(const CInv &inv);

/** Find the last common ancestor two blocks have.
 *  Both pa and pb must be non-NULL. */
CBlockIndex *LastCommonAncestor(CBlockIndex *pa, CBlockIndex *pb)
{
    if (pa->nHeight > pb->nHeight)
    {
        pa = pa->GetAncestor(pb->nHeight);
    }
    else if (pb->nHeight > pa->nHeight)
    {
        pb = pb->GetAncestor(pa->nHeight);
    }

    while (pa != pb && pa && pb)
    {
        pa = pa->pprev;
        pb = pb->pprev;
    }

    // Eventually all chain branches meet at the genesis block.
    assert(pa == pb);
    return pa;
}

void CRequestManager::InitializeNodeState(const CNode *pnode)
{
    WRITELOCK(cs_requestmanager);
    mapNodeState.emplace_hint(mapNodeState.end(), std::piecewise_construct, std::forward_as_tuple(pnode->GetId()),
        std::forward_as_tuple(pnode->addr, pnode->GetAddrName()));
    mapNumBlocksInFlight.emplace(pnode->GetId(), 0);
}

void CRequestManager::RemoveNodeState(const NodeId id)
{
    WRITELOCK(cs_requestmanager);
    mapNodeState.erase(id);
    mapNumBlocksInFlight.erase(id);
    for (auto iter = mapBlocksInFlight.begin(); iter != mapBlocksInFlight.end();)
    {
        if (iter->second.sources.count(id) && iter->second.sources.size() == 1)
        {
            iter = mapBlocksInFlight.erase(iter);
        }
        else
        {
            ++iter;
        }
    }
    if (headerPeerId.load() == id)
    {
        headerPeerSelected.store(false);
        headerPeerId.store(-1);
    }
}

void CRequestManager::RequestInitialBlockAvailability(CNode* pnode)
{
    NodeId nodeid = pnode->GetId();
    WRITELOCK(cs_requestmanager);
    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(nodeid);
    if(iter == mapNodeState.end())
    {
        return;
    }
    CNodeState* state = &iter->second;

    if (g_chainman.IsInitialBlockDownload() && !state->fRequestedInitialBlockAvailability && state->pindexBestKnownBlock == nullptr && !fReindex && !fImporting)
    {
        if (!pnode->fClient)
        {
            state->fRequestedInitialBlockAvailability = true;
            // We only want one single header so we pass a null CBlockLocator.
            g_connman->PushMessage(pnode, NetMsgType::GETHEADERS, CBlockLocator(),
                g_chainman.pindexBestHeader.load()->GetBlockHash());
            LogPrint("net", "Requesting header for initial blockavailability, peer=%li block=%s height=%d\n",
                nodeid, g_chainman.pindexBestHeader.load()->GetBlockHash().ToString(),
                g_chainman.pindexBestHeader.load()->nHeight);
        }
    }
}

/** Check whether the last unknown block a peer advertized is not yet known. */
void CRequestManager::ProcessBlockAvailability(NodeId nodeid)
{
    RECURSIVEREADLOCK(g_chainman.cs_mapBlockIndex);
    WRITELOCK(cs_requestmanager);

    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(nodeid);
    if(iter == mapNodeState.end())
    {
        return;
    }
    CNodeState* state = &iter->second;

    if (!state->hashLastUnknownBlock.IsNull())
    {
        CBlockIndex *pindex = g_chainman.LookupBlockIndex(state->hashLastUnknownBlock);
        if (pindex && pindex->nChainWork > 0)
        {
            if (state->pindexBestKnownBlock == NULL || pindex->nChainWork >= state->pindexBestKnownBlock->nChainWork)
            {
                state->pindexBestKnownBlock = pindex;
            }
            state->hashLastUnknownBlock.SetNull();
        }
    }
}

// TODO : currently needs cs_mapBlockIndex before locking this, should fix that
void CRequestManager::UpdateBlockAvailability(NodeId nodeid, const uint256 &hash)
{
    CBlockIndex *pindex = g_chainman.LookupBlockIndex(hash);
    ProcessBlockAvailability(nodeid);
    WRITELOCK(cs_requestmanager);
    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(nodeid);
    if(iter == mapNodeState.end())
    {
        return;
    }
    CNodeState* state = &iter->second;
    if (pindex && pindex->nChainWork > 0)
    {
        // An actually better block was announced.
        if (state->pindexBestKnownBlock == NULL || pindex->nChainWork >= state->pindexBestKnownBlock->nChainWork)
        {
            LogPrint("net", "updated peer %d best known block \n", nodeid);
            state->pindexBestKnownBlock = pindex;
        }
    }
    else
    {
        LogPrint("net", "updated peer %d hash last unknown block \n", nodeid);
        // An unknown block was announced; just assume that the latest one is the best one.
        state->hashLastUnknownBlock = hash;
    }
}

bool CRequestManager::PeerHasHeader(const NodeId nodeid, const CBlockIndex *pindex)
{
    READLOCK(cs_requestmanager);
    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(nodeid);
    if(iter == mapNodeState.end())
    {
        return false;
    }
    CNodeState* state = &iter->second;
    if (state->pindexBestKnownBlock && pindex == state->pindexBestKnownBlock->GetAncestor(pindex->nHeight))
    {
        return true;
    }
    if (state->pindexBestHeaderSent && pindex == state->pindexBestHeaderSent->GetAncestor(pindex->nHeight))
    {
        return true;
    }
    return false;
}

void CRequestManager::MarkBlockAsInFlight(NodeId nodeid, const uint256 &hash)
{
    WRITELOCK(cs_requestmanager);
    std::map<uint256, QueuedBlock>::iterator itInFlight = mapBlocksInFlight.find(hash);
    if (itInFlight != mapBlocksInFlight.end())
    {
        // adding a source to a block already in flight
        itInFlight->second.nDownloadStartTime = GetTime();
        itInFlight->second.sources.emplace(nodeid);
    }
    else
    {
        // new block
        mapBlocksInFlight[hash] = {GetTime(), {nodeid}};
    }
    if (mapNumBlocksInFlight.count(nodeid) != 0)
    {
        mapNumBlocksInFlight[nodeid] += 1;
    }
}

void CRequestManager::UpdatePreferredDownload(CNode *node)
{
    WRITELOCK(cs_requestmanager);
    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(node->GetId());
    if(iter == mapNodeState.end())
    {
        return;
    }
    CNodeState* state = &iter->second;

    nPreferredDownload.fetch_sub(state->fPreferredDownload);

    // Whether this node should be marked as a preferred download node.
    // we allow downloads from inbound nodes; this may have been limited in the past to stop attackers from connecting
    // and offering a bad chain. However, we are connecting to multiple nodes and so can choose the most work
    // chain on that basis.
    state->fPreferredDownload = !node->fOneShot && !node->fClient;

    nPreferredDownload.fetch_add(state->fPreferredDownload);
}

// Returns a bool indicating whether we requested this block.
bool CRequestManager::MarkBlockAsReceived(const uint256 &hash)
{
    WRITELOCK(cs_requestmanager);
    std::map<uint256, QueuedBlock>::iterator itInFlight = mapBlocksInFlight.find(hash);
    if (itInFlight != mapBlocksInFlight.end())
    {
        for (const NodeId &source : itInFlight->second.sources)
        {
            if (mapNumBlocksInFlight.count(source))
            {
                mapNumBlocksInFlight[source] -= 1;
            }
        }
        mapBlocksInFlight.erase(itInFlight);
        return true;
    }
    return false;
}

void CRequestManager::SetBestHeaderSent(NodeId nodeid, CBlockIndex* pindex)
{
    WRITELOCK(cs_requestmanager);
    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(nodeid);
    if(iter == mapNodeState.end())
    {
        return;
    }
    iter->second.pindexBestHeaderSent = pindex;
}

bool CRequestManager::GetNodeStateStats(NodeId nodeid, CNodeStateStats &stats)
{
    RECURSIVEREADLOCK(g_chainman.cs_mapBlockIndex);
    READLOCK(cs_requestmanager);
    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(nodeid);
    if(iter == mapNodeState.end())
    {
        return false;
    }
    CNodeState* state = &iter->second;

    stats.nSyncHeight = state->pindexBestKnownBlock ? state->pindexBestKnownBlock->nHeight : -1;
    stats.nCommonHeight = state->pindexLastCommonBlock ? state->pindexLastCommonBlock->nHeight : -1;

    for (const auto &queue : mapBlocksInFlight)
    {
        if (queue.second.sources.count(nodeid))
        {
            // lookup block by hash to find height
            CBlockIndex *pindex = g_chainman.LookupBlockIndex(queue.first);
            if (pindex)
            {
                stats.vHeightInFlight.push_back(pindex->nHeight);
            }
        }
    }
    return true;
}

bool CRequestManager::GetPreferHeaders(CNode *node)
{
    READLOCK(cs_requestmanager);
    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(node->GetId());
    if(iter == mapNodeState.end())
    {
        return false;
    }
    return iter->second.fPreferHeaders;
}

void CRequestManager::SetPreferHeaders(CNode *node)
{
    WRITELOCK(cs_requestmanager);
    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(node->GetId());
    if(iter == mapNodeState.end())
    {
        return;
    }
    iter->second.fPreferHeaders = true;
}

int CRequestManager::GetBlocksInFlight(NodeId nodeid)
{
    READLOCK(cs_requestmanager);
    std::map<NodeId, int16_t>::iterator iter = mapNumBlocksInFlight.find(nodeid);
    if(iter == mapNumBlocksInFlight.end())
    {
        return 0;
    }
    return iter->second;
}

void CRequestManager::StartDownload(CNode* node)
{
    WRITELOCK(cs_requestmanager);
    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(node->GetId());
    if(iter == mapNodeState.end())
    {
        return;
    }
    CNodeState* state = &iter->second;

    // Download if this is a nice peer, or we have no nice peers and this one
    // might do.
    bool fFetch = state->fPreferredDownload || (nPreferredDownload.load() == 0 && !node->fOneShot);


    if (!state->fSyncStarted && !node->fClient && !fImporting && !fReindex)
    {
        // only download headers from one peer at a time unless we are close to today
        if ( (headerPeerSelected.load() == false && fFetch) ||
            g_chainman.pindexBestHeader.load()->GetBlockTime() > GetAdjustedTime() - 24 * 60 * 60)
        {
            state->fSyncStarted = true;
            const CBlockIndex *pindexStart = g_chainman.pindexBestHeader;
            /**
             * If possible, start at the block preceding the currently best
             * known header. This ensures that we always get a non-empty list of
             * headers back as long as the peer is up-to-date. With a non-empty
             * response, we can initialise the peer's known best block. This
             * wouldn't be possible if we requested starting at pindexBestHeader
             * and got back an empty response.
             */
            if (pindexStart->pprev)
            {
                pindexStart = pindexStart->pprev;
            }
            if (pindexStart->nHeight < node->nStartingHeight)
            {
                LogPrint("net", "initial getheaders (%d) to peer=%d (startheight:%d)\n", pindexStart->nHeight, node->id,
                    node->nStartingHeight);
                g_connman->PushMessage(
                    node, NetMsgType::GETHEADERS, g_chainman.chainActive.GetLocator(pindexStart), uint256());
                headerPeerSelected.store(true);
                headerPeerId.store(node->GetId());
            }
        }
    }
}

bool CRequestManager::IsBlockInFlight(const uint256 &hash)
{
    READLOCK(cs_requestmanager);
    return mapBlocksInFlight.count(hash);
}

void CRequestManager::TrackTxRelay(const CTransaction &tx)
{
    CInv inv(MSG_TX, tx.GetId());
    LOCK(cs_mapRelay);
    // Expire old relay messages
    while (!vRelayExpiration.empty() && vRelayExpiration.front().first < GetTime())
    {
        mapRelay.erase(vRelayExpiration.front().second);
        vRelayExpiration.pop_front();
    }
    // Save original serialized message so newer versions are preserved
    auto ret = mapRelay.emplace(inv.hash, tx);
    if (ret.second)
    {
        vRelayExpiration.push_back(std::make_pair(GetTime() + 15 * 60, ret.first));
    }
}

bool CRequestManager::FindAndPushTx(CNode* node, const uint256 &hash)
{
    LOCK(cs_mapRelay);
    // Send stream from relay memory
    auto mi = mapRelay.find(hash);
    if (mi != mapRelay.end())
    {
        g_connman->PushMessage(node, NetMsgType::TX, mi->second);
        return true;
    }
    return false;
}

void CRequestManager::SetPeerFirstHeaderReceived(CNode* node, CBlockIndex* pindexLast)
{
    WRITELOCK(cs_requestmanager);
    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(node->GetId());
    if(iter == mapNodeState.end())
    {
        return;
    }
    CNodeState* state = &iter->second;
    // During the initial peer handshake we must receive the initial headers which should be greater
    // than or equal to our block height at the time of requesting GETHEADERS. This is because the peer has
    // advertised a height >= to our own. Furthermore, because the headers max returned is as much as 2000 this
    // could not be a mainnet re-org.
    if (!state->fFirstHeadersReceived)
    {
        // We want to make sure that the peer doesn't just send us any old valid header. The block height of the
        // last header they send us should be equal to our block height at the time we made the GETHEADERS
        // request.
        if (pindexLast && state->nFirstHeadersExpectedHeight <= pindexLast->nHeight)
        {
            state->fFirstHeadersReceived = true;
            LogPrint("net", "Initial headers received for peer=%d\n", node->GetId());
        }
    }
}

void CRequestManager::SetPeerSyncStartTime(CNode* node)
{
    int64_t now = GetTime();
    WRITELOCK(cs_requestmanager);
    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(node->GetId());
    if(iter == mapNodeState.end())
    {
        return;
    }
    CNodeState* state = &iter->second;
    state->nSyncStartTime = now; // reset the time because more headers needed
}

std::vector<NodeId> CRequestManager::UpdateBestKnowBlockAll(CBlockIndex* pindexLast)
{
    std::vector<NodeId> nodes;
    READLOCK(cs_requestmanager);
    for (auto &state : mapNodeState)
    {
        if (state.second.pindexBestKnownBlock == nullptr || pindexLast->nChainWork > state.second.pindexBestKnownBlock->nChainWork)
        {
            nodes.push_back(state.first);
        }
    }
    return nodes;
}

void CRequestManager::RequestNextBlocksToDownload(CNode* node)
{
    NodeId nodeid = node->GetId();
    uint16_t nBlocksInFlight = 0;
    {
        READLOCK(cs_requestmanager);
        std::map<NodeId, int16_t>::iterator iter = mapNumBlocksInFlight.find(nodeid);
        if(iter == mapNumBlocksInFlight.end())
        {
            return;
        }
        nBlocksInFlight = iter->second;
    }
    if (!node->fDisconnect && !node->fClient && nBlocksInFlight < MAX_BLOCKS_IN_TRANSIT_PER_PEER)
    {
        std::vector<CBlockIndex *> vToDownload;
        std::vector<CInv> vToFetchNew;
        FindNextBlocksToDownload(nodeid, MAX_BLOCKS_IN_TRANSIT_PER_PEER - nBlocksInFlight, vToDownload);
        std::map<uint256, QueuedBlock>::iterator itInFlight;
        for (CBlockIndex *pindex : vToDownload)
        {
            CInv inv(MSG_BLOCK, pindex->GetBlockHash());
            if (!AlreadyHaveBlock(inv))
            {
                vToFetchNew.push_back(std::move(inv));
            }
        }
        if (vToFetchNew.empty() == false)
        {
            g_connman->PushMessage(node, NetMsgType::GETDATA, vToFetchNew);
            for (auto &block : vToFetchNew)
            {
                MarkBlockAsInFlight(nodeid, block.hash);
            }
        }
    }
}

// Update pindexLastCommonBlock and add not-in-flight missing successors to vBlocks, until it has
// at most count entries.
void CRequestManager::FindNextBlocksToDownload(const NodeId &nodeid, const size_t &count, std::vector<CBlockIndex *> &vBlocks)
{
    if (count == 0)
    {
        return;
    }
    vBlocks.reserve(vBlocks.size() + count);

    // Make sure pindexBestKnownBlock is up to date, we'll need it.
    ProcessBlockAvailability(nodeid);

    RECURSIVEREADLOCK(g_chainman.cs_mapBlockIndex);
    WRITELOCK(cs_requestmanager);
    std::map<NodeId, CNodeState>::iterator iter = mapNodeState.find(nodeid);
    if(iter == mapNodeState.end())
    {
        return;
    }
    CNodeState* state = &iter->second;

    if (state->pindexBestKnownBlock == nullptr ||
        state->pindexBestKnownBlock->nChainWork < g_chainman.chainActive.Tip()->nChainWork)
    {
        // This peer has nothing interesting.
        return;
    }

    if (state->pindexLastCommonBlock == nullptr)
    {
        // Bootstrap quickly by guessing a parent of our best tip is the forking point.
        // Guessing wrong in either direction is not a problem.
        state->pindexLastCommonBlock =
            g_chainman.chainActive[std::min(state->pindexBestKnownBlock->nHeight, g_chainman.chainActive.Height())];
    }

    // If the peer reorganized, our previous pindexLastCommonBlock may not be an ancestor
    // of its current tip anymore. Go back enough to fix that.
    state->pindexLastCommonBlock = LastCommonAncestor(state->pindexLastCommonBlock, state->pindexBestKnownBlock);
    if (state->pindexLastCommonBlock == state->pindexBestKnownBlock)
    {
        return;
    }

    std::vector<CBlockIndex *> vToFetch;
    CBlockIndex *pindexWalk = state->pindexLastCommonBlock;

    int nMaxHeight = state->pindexBestKnownBlock->nHeight;
    while (pindexWalk->nHeight < nMaxHeight)
    {
        // Read up to 128 (or more, if more blocks than that are needed) successors of pindexWalk (towards
        // pindexBestKnownBlock) into vToFetch. We fetch 128, because CBlockIndex::GetAncestor may be as expensive
        // as iterating over ~100 CBlockIndex* entries anyway.
        int nToFetch = std::min((size_t)(nMaxHeight - pindexWalk->nHeight), count - vBlocks.size());
        if (nToFetch == 0)
        {
            break;
        }
        vToFetch.resize(nToFetch);
        pindexWalk = state->pindexBestKnownBlock->GetAncestor(pindexWalk->nHeight + nToFetch);
        vToFetch[nToFetch - 1] = pindexWalk;
        for (unsigned int i = nToFetch - 1; i > 0; i--)
        {
            vToFetch[i - 1] = vToFetch[i]->pprev;
        }

        // Iterate over those blocks in vToFetch (in forward direction), adding the ones that
        // are not yet downloaded and not in flight to vBlocks. In the mean time, update
        // pindexLastCommonBlock as long as all ancestors are already downloaded, or if it's
        // already part of our chain (and therefore don't need it even if pruned).
        int64_t now = GetTime();
        for (CBlockIndex *pindex : vToFetch)
        {
            if (vBlocks.size() >= count)
            {
                return;
            }
            uint256 blockHash = pindex->GetBlockHash();
            std::map<uint256, QueuedBlock>::iterator itInFlight = mapBlocksInFlight.find(blockHash);
            if (itInFlight != mapBlocksInFlight.end())
            {
                // rerequest from another source
                if (itInFlight->second.nDownloadStartTime + DEFAULT_BLOCK_REREQUEST_TIMEOUT < now)
                {
                    if (itInFlight->second.sources.count(nodeid) == 0)
                    {
                        vBlocks.push_back(pindex);
                    }
                }
                continue;
            }
            if (!pindex->IsValid(BLOCK_VALID_TREE))
            {
                // We consider the chain that this peer is on invalid.
                return;
            }
            if (pindex->nStatus & BLOCK_HAVE_DATA || g_chainman.chainActive.Contains(pindex))
            {
                if (pindex->nChainTx)
                {
                    state->pindexLastCommonBlock = pindex;
                }
            }
            else
            {
                // Return if we've reached the end of the number of blocks we can download for this peer.
                vBlocks.push_back(pindex);
            }
        }
    }
}
